waitUntil {!(isNull player)};
waitUntil {player==player};

switch (side player) do {

	case WEST:
	{
	player createDiaryRecord ["Diary",["WYTYCZNE - BLUFOR","Waszym zadaniem jest uratowanie zakładników. Z danych pozyskanych od informatora wiemy, że OPFOR chce wymienić ich za ciężarówkę INDFORu. Możecie spróbować zaatakować OPFOR lub poczekać do czasu wymiany i odbić zakładników z rąk INDFORU. Miejsce wymiany nieznane, musiscie zlokalizować przeciwników samodzielnie.<br/><br/>Wszyscy operatorzy posiadają radia AN/PRC-152. W śmigłowcu znajduje się radio długie AN/PRC-117F w racku AN/VRC-103.<br/><br/>Według informatora OPFOR będzie kontaktować się z INDFORem na nieznanej dokładnie częstotliwości wywoławczej między 30 a 40 Mhz, którą można podsłuchać przy pomocy AN/PRC-152 i AN/PRC-117F. Wymaga to jednak odpowiedniej konfiguracji kanału w ustawieniach radiostacji."]];
	};
	
	case EAST:
	{
	player createDiaryRecord ["Diary",["WYTYCZNE - OPFOR","INDFOR chce pozyskać zakładników w zamian za posiadaną przez nich ciężarówkę z bronią przeciwlotniczą. Możecie dokonać wymiany fair lub spróbować zatrzymać zakładników dla siebie i przechwycić ciężarówkę podstępem.<br/><br/>W celu skontaktowania się z INDFORem dowódca posiada radio długie SEM70. Częstotliwość wywoławcza - 35,50 MHz.<br/><br/>Radia średnie (SEM52) posiadają tylko dowódcy sekcji oraz SL."]];
	};
	
	case RESISTANCE:
	{
	player createDiaryRecord ["Diary",["WYTYCZNE - INDFOR","OPFOR chce wymienić zakładników na posiadaną przez was ciężarówkę z bronią przeciwlotniczą. Możecie dokonać wymiany fair lub spróbować zatrzymać ciężarówkę dla siebie i przechwycić zakładników podstępem.<br/><br/>W celu skontaktowania się z OPFORem dowódca posiada radio długie AN/PRC-77. Częstotliwość wywoławcza - 35,50 MHz.<br/><br/>Wszyscy posiadacie radia krótkie AN/PRC-343 do komunikacji w obrębie grupy."]];
	};
	
	case CIVILIAN:
	{
	player createDiaryRecord ["Diary",["WYTYCZNE - ZAKŁADNICY","Waszym podstawowym zadaniem jest przeżyć. Możecie próbować grać na zwłokę aby zwięszyć szanse BLUFORu na uratowanie was lub podjąć próbę ucieczki."]];
	};
	
};