class AF {

	/*
		Ares Modules

		Moduły zeusowe do różnych rzeczy
	*/
	class aresModules {
		class aresModulesEnvironmentLightsBlackout {
			postInit = 1;
		};
		class aresModulesFaggots {};
		class aresModulesSoundSelectAndSpawn {
			postInit = 1;
		};
		class aresModulesZeusMoveAfterCamera {
			postInit = 1;
		};
		class respawnAllDead {
			postInit = 1;
		};
	};

	/*
		Cache

		Funkcje do cache jednostek
	*/
	class cache {
		class cacheInit {};
		class cacheStop {};
	};

	/*
		Damage

		Obsługa obrażen gracza etc.
	*/
	class damage {
		class addDamageHandlers {};
		class checkVitalsLoop {};
		class createHitEffect {};
		class handleDamageHc {};
		class initializeDamage {
			preInit = 1;
		};
	};

	/*
		dialogs

		Funkcje pod GUI
	*/
	class dialogs {
		class groupRenameGUI {};
		class groupRenameList {};
		class setViewDistance {};
		class viewDistanceGUI {};
	};

	/*
		Faggots

		Loguje informacje o incydentach (strzelanie, granaty itd.)
	*/
	class faggots {
		class faggotsInfo {};
		class faggotsLog {};
		class faggotsPunish {};
	};

	/*
		friendlyTracker

		Markery wskazujące sojusznicze jednostki
	*/
	class friendlyTracker {
		class friendlyTrackerInit {};
		class friendlyTrackerLoop {};
		class friendlyTrackerStop {};
	};

	/*
		init

		Funkcje inicjalizujące
	*/
	class init {
		class main {
			postInit = 1;
		};
		class player {
			postInit = 1;
		};
		class server {
			postInit = 1;
		};
	};

	/*
		Player

		Funkcje graczy
	*/

	class player {
		class playerActions {};
		class playerEHs {
			postInit = 1;
		};
		class playerKilledSpectator {};
	};

	/*
		Respawn

		Funkcje respawnu
	*/
	class respawn {
		class playerTicketsCheck {};
		class playerTicketsUpdate {};
		class respawnInit {
			postInit = 1;
		};
	};

	/*
		Server

		Funkcje serwerowe
	*/
	class server {
		class addToZeus {
			postInit = 1;
		};
	};

	/*
		Crash Teleport

		Teleportacja po wyrzuceniu z serwera
	*/
	class crashTeleport {
		class crashTeleportCheck {};
		class crashTeleportCountdown {};
		class crashTeleportDialog {};
		class crashTeleportPlayer {};
		class crashTeleportServer {};
		class crashTeleportServerHandler {
			postInit = 1;
		};
	};

	/*
		Units

		Funkcje jednostek ai/graczy
	*/
	class units {
		class AI_Init {};
		class eventHandlers {};
		class killUncons {};
	};

	/*
		Util

		Funkcje pomocnicze różnego przeznaczenia
	*/
	class util {
		class anim {};
		class getAllLocalUnits {};
		class getCommandPwd {};
		class getCurrentZoom {};
		class localLog {};
		class restartAfterMissionEnd {};
		class serverLog {};
		class toggleNearLights {};
		class utilGlobals {
			preInit = 1;
		};
	};
};