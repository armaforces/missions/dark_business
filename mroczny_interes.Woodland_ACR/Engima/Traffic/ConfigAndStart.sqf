/* 
 * This file contains parameters to config and function call to start an instance of
 * traffic in the mission. The file is edited by the mission developer.
 *
 * See file Engima\Traffic\Documentation.txt for documentation and a full reference of 
 * how to customize and use Engima's Traffic.
 */
 
 private ["_parameters"];

// Set traffic parameters.
_parameters = [
	["SIDE", civilian],
	["VEHICLES", ["CUP_C_Golf4_random_Civ", "CUP_C_Datsun", "C_Van_01_fuel_F", "C_Hatchback_01_F", "C_Hatchback_01_sport_F", "C_Offroad_02_unarmed_F", "C_Truck_02_fuel_F", "C_Truck_02_box_F", "C_Truck_02_transport_F", "C_Truck_02_covered_F", "C_Offroad_01_F", "C_Offroad_01_repair_F", "C_SUV_01_F", "C_Van_01_transport_F", "C_Van_01_box_F", "C_Van_02_medevac_F", "C_Van_02_vehicle_F", "C_Van_02_service_F", "C_Van_02_transport_F", "RHS_Ural_Civ_01", "RHS_Ural_Open_Civ_01", "RHS_Ural_Civ_03", "RHS_Ural_Open_Civ_03", "RHS_Ural_Civ_02", "RHS_Ural_Open_Civ_02"]],
	["VEHICLES_COUNT", 5],
	["MIN_SPAWN_DISTANCE", 800],
	["MAX_SPAWN_DISTANCE", 1600],
	["MIN_SKILL", 0.6],
	["MAX_SKILL", 1.0],
	["DEBUG", false]
];

// Start an instance of the traffic
_parameters spawn ENGIMA_TRAFFIC_StartTraffic;
