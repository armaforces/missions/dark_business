AF_loadout_indfor = [
	["Bliski Wschód","Afganistan ANA '14","Ajvar",[
		["I_soldier_F","LOP_U_AA_Fatigue_01","LOP_V_CarrierRig_OLV","","rhssaf_helmet_m97_olive_nocamo","KA_M16A2",["","","",""],["rhs_mag_30Rnd_556x45_M855_Stanag_Tracer_Red"],"",["","","",""],[]],
		["I_crew_F","LOP_U_AA_Fatigue_01_slv","rhssaf_vest_md98_woodland_fix","","rhsusf_cvc_alt_helmet","rhs_weap_aks74u",["rhs_acc_pgs64_74un","","",""],["rhs_30Rnd_545x39_7N10_2mag_plum_AK"],"rhs_weap_tt33",["","","",""],["rhs_mag_762x25_8"]],
		["I_Soldier_TL_F","LOP_U_AA_Fatigue_02_slv","LOP_V_CarrierRig_OLV","VSM_M81_Backpack_Compact","rhssaf_helmet_m97_olive_nocamo_black_ess_bare","rhs_weap_m16a4_carryhandle_M203",["","","",""],["rhs_mag_30Rnd_556x45_M855_Stanag_Tracer_Red","rhs_mag_M441_HE"],"rhs_weap_tt33",["","","",""],["rhs_mag_762x25_8"]],
		["I_Soldier_AR_F","LOP_U_AA_Fatigue_01","LOP_V_CarrierRig_WDL","B_AssaultPack_khk","rhsgref_helmet_pasgt_woodland","rhs_weap_pkm",["","","",""],["rhssaf_250Rnd_762x54R"],"rhs_weap_tt33",["","","",""],["rhs_mag_762x25_8"]],
		["I_medic_F","LOP_U_AA_Fatigue_02","LOP_V_CarrierRig_OLV","B_TacticalPack_oli","rhssaf_helmet_m97_olive_nocamo_black_ess","KA_M16A2",["","","",""],["rhs_mag_30Rnd_556x45_M855_Stanag_Tracer_Red"],"rhs_weap_tt33",["","","",""],["rhs_mag_762x25_8"]],
		["I_G_medic_F","LOP_U_AA_Fatigue_01_slv","LOP_V_CarrierRig_OLV","VSM_M81_carryall","rhssaf_helmet_m97_olive_nocamo","KA_M16A2",["","","",""],["rhs_mag_30Rnd_556x45_M855_Stanag_Tracer_Red"],"rhs_weap_tt33",["","","",""],["rhs_mag_762x25_8"]],
		["I_Soldier_SL_F","LOP_U_AA_Fatigue_02_slv","LOP_V_CarrierRig_WDL","tfw_ilbe_gr","rhsusf_ach_helmet_M81","rhs_weap_m16a4_carryhandle_M203",["","","",""],["rhs_mag_30Rnd_556x45_M855_Stanag_Tracer_Red","rhs_mag_M441_HE"],"rhs_weap_tt33",["","","",""],["rhs_mag_762x25_8"]],
		["I_officer_F","LOP_U_AA_Fatigue_02_slv","LOP_V_Carrier_ANA","tfw_ilbe_coy","LOP_H_6B27M_ess_ANA","",["","","",""],[],"rhsusf_weap_m9",["","","",""],["rhsusf_mag_15Rnd_9x19_JHP"]]]
	],
	["Bliski Wschód","Afganistan Policja ANP '14","Ajvar",[
		["I_soldier_F","LOP_U_AA_Fatigue_03","V_TacVest_blk","","rhssaf_helmet_m97_black_nocamo","rhs_weap_akm",["rhs_acc_dtkakm","","",""],["rhs_30Rnd_762x39mm_bakelite_tracer"],"",["","","",""],[]],
		["I_crew_F","LOP_U_AA_Fatigue_03_slv","rhssaf_vest_md98_woodland_fix","","rhs_tsh4","rhs_weap_akms",["rhs_acc_dtkakm","","",""],["rhs_30Rnd_762x39mm_bakelite"],"rhs_weap_tt33",["","","",""],["rhs_mag_762x25_8"]],
		["I_Soldier_TL_F","LOP_U_AA_Fatigue_03_slv","V_TacVest_blk","","rhssaf_helmet_m97_black_nocamo_black_ess","rhs_weap_akm_gp25",["rhs_acc_dtkakm","","",""],["rhs_30Rnd_762x39mm_tracer","rhs_VOG25"],"rhs_weap_tt33",["","","",""],["rhs_mag_762x25_8"]],
		["I_Soldier_AR_F","LOP_U_AA_Fatigue_03_slv","V_TacVest_blk","B_AssaultPack_blk","rhssaf_helmet_m97_black_nocamo","rhs_weap_pkm",["","","",""],["rhssaf_250Rnd_762x54R"],"rhs_weap_tt33",["","","",""],["rhs_mag_762x25_8"]],
		["I_medic_F","LOP_U_AA_Fatigue_03_slv","V_TacVest_blk","B_TacticalPack_blk","rhssaf_helmet_m97_black_nocamo_black_ess","rhs_weap_akms",["rhs_acc_dtkakm","","",""],["rhs_30Rnd_762x39mm_bakelite_tracer"],"rhs_weap_tt33",["","","",""],["rhs_mag_762x25_8"]],
		["I_G_medic_F","LOP_U_AA_Fatigue_03","V_TacVest_blk","B_Carryall_oli","rhssaf_helmet_m97_black_nocamo","rhs_weap_akm",["rhs_acc_dtkakm","","",""],["rhs_30Rnd_762x39mm_bakelite_tracer"],"rhs_weap_tt33",["","","",""],["rhs_mag_762x25_8"]],
		["I_Soldier_SL_F","LOP_U_AA_Fatigue_03_slv","V_TacVest_blk","B_AssaultPack_blk","rhssaf_helmet_m97_black_nocamo_black_ess_bare","rhs_weap_akm_gp25",["rhs_acc_dtkakm","","",""],["rhs_30Rnd_762x39mm","rhs_VOG25"],"rhs_weap_tt33",["","","",""],["rhs_mag_762x25_8"]],
		["I_officer_F","LOP_U_AA_Fatigue_03","LOP_V_Carrier_TAN","tfw_ilbe_coy","LOP_H_Fieldcap_BLU","",["","","",""],[],"rhsusf_weap_m9",["","","",""],["rhsusf_mag_15Rnd_9x19_JHP"]]]
	],
	["Bliski Wschód","PMC '08","Ajvar",[
		["I_soldier_F","U_BG_Guerilla2_1","VSM_MBSS_PACA_CB","","H_Cap_blk","rhs_weap_m4",["","rhsusf_acc_M952V","rhsusf_acc_compm4","rhsusf_acc_grip3"],["rhs_mag_30Rnd_556x45_M855_Stanag_Tracer_Red"],"rhsusf_weap_m9",["","","",""],["rhsusf_mag_15Rnd_9x19_JHP"]],
		["I_crew_F","LOP_U_PMC_Fatigue_04","rhssaf_vest_md98_woodland_fix","","rhsusf_cvc_green_alt_helmet","rhs_weap_m4_carryhandle",["","","SMA_eotech552",""],["rhs_mag_30Rnd_556x45_M855A1_Stanag"],"KA_P226_Black",["","","",""],["KA_P226_15Rnd_9x19_FMJ_Mag"]],
		["I_Soldier_TL_F","U_BG_Guerilla2_2","VSM_MBSS_PACA","","rhsusf_bowman_cap","rhs_weap_m4_m203S",["","SMA_SFFL_TAN","rhsusf_acc_ACOG",""],["rhs_mag_30Rnd_556x45_M855A1_Stanag","rhs_mag_M441_HE"],"rhsusf_weap_m9",["","","",""],["rhsusf_mag_15Rnd_9x19_JHP"]],
		["I_Soldier_AR_F","U_BG_Guerilla2_3","VSM_RAV_MG_OGA","B_TacticalPack_oli","VSM_BackwardsHat_Peltor_Sero","rhs_weap_m249_pip",["","","",""],["rhsusf_200rnd_556x45_mixed_box"],"rhsusf_weap_m9",["","","",""],["rhsusf_mag_15Rnd_9x19_JHP"]],
		["I_medic_F","U_I_G_resistanceLeader_F","VSM_LBT1961_GRN","B_TacticalPack_oli","H_Cap_headphones","rhs_weap_m4a1_carryhandle",["","rhsusf_acc_M952V","SMA_eotech552_3XDOWN",""],["rhs_mag_30Rnd_556x45_M855A1_Stanag"],"KA_Px4",["","","",""],["KA_Px4_17Rnd_9x19_FMJ_Mag"]],
		["I_G_medic_F","U_BG_Guerilla2_3","VSM_LBT1961_CB","VSM_OGA_carryall","rhssaf_booniehat_woodland","rhs_weap_m4a1_carryhandle",["","","",""],["rhs_mag_30Rnd_556x45_M855A1_Stanag"],"rhsusf_weap_glock17g4",["","","",""],["rhsusf_mag_17Rnd_9x19_JHP"]],
		["I_Soldier_SL_F","LOP_U_AFR_Fatigue_02","VSM_MBSS_TAN","UK3CB_BAF_B_Bergen_OLI_SL_A","VSM_Bowman_cap_Tan","rhs_weap_m4_m203",["","rhsusf_acc_anpeq15side","rhsusf_acc_compm4",""],["rhs_mag_30Rnd_556x45_M855A1_Stanag","rhs_mag_M441_HE"],"rhsusf_weap_m9",["","","",""],["rhsusf_mag_15Rnd_9x19_JHP"]],
		["I_officer_F","U_Rangemaster","VSM_MBSS_PACA","UK3CB_BAF_B_Bergen_OLI_SL_A","VSM_Bowman_cap_Green","",["","","",""],[],"rhsusf_weap_glock17g4",["","","",""],["rhsusf_mag_17Rnd_9x19_JHP"]]]
	],
	["EU Woodland","Szwajcaria '90","Ajvar",[
		["I_soldier_F","rhsgref_uniform_alpenflage","rhsgref_alice_webbing","","rhsgref_helmet_pasgt_olive","KA_SG_550",["","","",""],["KA_SIG_30rnd_M196_Tracer_Red_mag"],"",["","","",""],[]],
		["I_crew_F","rhsgref_uniform_alpenflage","rhsgref_alice_webbing","","rhsusf_cvc_green_alt_helmet","KA_SG_552",["","","",""],["KA_SIG_30rnd_M196_Tracer_Red_mag"],"KA_P226_Black",["","","",""],["KA_P226_15Rnd_9x19_FMJ_Mag"]],
		["I_Soldier_TL_F","rhsgref_uniform_alpenflage","rhsgref_alice_webbing","","rhsgref_helmet_pasgt_olive","KA_SG_550",["","","",""],["KA_SIG_30rnd_M196_Tracer_Red_mag"],"KA_P226",["","","",""],["KA_P226_15Rnd_9x19_FMJ_Mag"]],
		["I_Soldier_AR_F","rhsgref_uniform_alpenflage","rhsgref_alice_webbing","B_TacticalPack_oli","rhsgref_helmet_pasgt_olive","rhs_weap_m249",["","","",""],["rhsusf_200rnd_556x45_M855_mixed_box"],"KA_P226",["","","",""],["KA_P226_15Rnd_9x19_FMJ_Mag"]],
		["I_medic_F","rhsgref_uniform_alpenflage","rhsgref_alice_webbing","B_TacticalPack_oli","rhsgref_helmet_pasgt_olive","KA_SG_550",["","","",""],["KA_SIG_30rnd_M196_Tracer_Red_mag"],"KA_P226",["","","",""],["KA_P226_15Rnd_9x19_FMJ_Mag"]],
		["I_G_medic_F","rhsgref_uniform_alpenflage","rhsgref_alice_webbing","rhssaf_alice_smb","rhsgref_helmet_pasgt_olive","KA_SG_550",["","","",""],["KA_SIG_30rnd_M196_Tracer_Red_mag"],"KA_P226",["","","",""],["KA_P226_15Rnd_9x19_FMJ_Mag"]],
		["I_Soldier_SL_F","rhsgref_uniform_alpenflage","rhsgref_alice_webbing","UK3CB_BAF_B_Bergen_OLI_SL_A","rhsgref_helmet_pasgt_olive","KA_SG_550",["","","",""],["KA_SIG_30rnd_M196_Tracer_Red_mag"],"KA_P226",["","","",""],["KA_P226_15Rnd_9x19_FMJ_Mag"]],
		["I_officer_F","rhsgref_uniform_alpenflage","rhsgref_alice_webbing","UK3CB_BAF_B_Bergen_OLI_SL_A","BWA3_Beret_Wach_blue","",["","","",""],[],"BWA3_P8",["","","",""],["BWA3_15Rnd_9x19_P8"]]]
	],
	["Serbia","M93 Oakleaf summer","Madin",[
		["I_soldier_F","rhssaf_uniform_m93_oakleaf_summer","rhssaf_vest_md99_md2camo_rifleman_fix","","rhssaf_helmet_m59_85_nocamo","rhs_weap_m70b3n",["","","",""],["rhs_30Rnd_762x39mm_tracer"],"rhs_weap_tt33",["","","",""],["rhs_mag_762x25_8"]],
		["I_crew_F","rhssaf_uniform_m93_oakleaf_summer","rhssaf_vest_md99_md2camo_rifleman_fix","","rhs_tsh4","rhs_weap_m70b3n",["","","",""],["rhs_30Rnd_762x39mm_tracer"],"rhs_weap_tt33",["","","",""],["rhs_mag_762x25_8"]],
		["I_Soldier_TL_F","rhssaf_uniform_m93_oakleaf_summer","rhssaf_vest_md12_digital_fix","","rhssaf_helmet_m97_oakleaf","rhs_weap_m70b3n_pbg40",["","","",""],["rhs_30Rnd_762x39mm_tracer","rhs_VOG25"],"rhs_weap_tt33",["","","",""],["rhs_mag_762x25_8"]],
		["I_Soldier_AR_F","rhssaf_uniform_m93_oakleaf_summer","rhssaf_vest_md99_md2camo_fix","rhssaf_kitbag_digital","rhssaf_helmet_m59_85_nocamo","rhs_weap_m84",["","","",""],["rhs_100Rnd_762x54mmR_green"],"rhs_weap_tt33",["","","",""],["rhs_mag_762x25_8"]],
		["I_medic_F","rhssaf_uniform_m93_oakleaf_summer","rhssaf_vest_md99_md2camo_rifleman_fix","rhs_assault_umbts","rhssaf_helmet_m97_oakleaf","rhs_weap_m70b3n",["","","",""],["rhs_30Rnd_762x39mm_tracer"],"rhs_weap_tt33",["","","",""],["rhs_mag_762x25_8"]],
		["I_G_medic_F","rhssaf_uniform_m93_oakleaf_summer","rhssaf_vest_md99_md2camo_rifleman_fix","B_Carryall_oli","rhssaf_helmet_m97_oakleaf","rhs_weap_m70b3n",["","","",""],["rhs_30Rnd_762x39mm_tracer"],"rhs_weap_tt33",["","","",""],["rhs_mag_762x25_8"]],
		["I_officer_F","rhssaf_uniform_m10_digital","rhssaf_vest_md12_digital","","rhssaf_beret_green","rhs_weap_m21s",["","","rhs_acc_pkas",""],["rhsgref_30rnd_556x45_m21"],"",["","","",""],[]],
		["I_Soldier_SL_F","rhssaf_uniform_m93_oakleaf_summer","rhssaf_vest_md12_digital_fix","rhs_assault_umbts","rhssaf_helmet_m97_oakleaf","rhs_weap_m70b3n_pbg40",["","","",""],["rhs_30Rnd_762x39mm_tracer","rhs_VOG25"],"rhs_weap_tt33",["","","",""],["rhs_mag_762x25_8"]]]
	],
	["Serbia","M93 Oakleaf","Madin",[
		["I_soldier_F","rhssaf_uniform_m93_oakleaf","rhssaf_vest_md99_md2camo_rifleman_fix","","rhssaf_helmet_m59_85_nocamo","rhs_weap_m70b3n",["","","",""],["rhs_30Rnd_762x39mm_tracer"],"rhs_weap_tt33",["","","",""],["rhs_mag_762x25_8"]],
		["I_crew_F","rhssaf_uniform_m93_oakleaf","rhssaf_vest_md99_md2camo_rifleman_fix","","rhs_tsh4","rhs_weap_m70b3n",["","","",""],["rhs_30Rnd_762x39mm_tracer"],"rhs_weap_tt33",["","","",""],["rhs_mag_762x25_8"]],
		["I_Soldier_TL_F","rhssaf_uniform_m93_oakleaf","rhssaf_vest_md12_digital_fix","","rhssaf_helmet_m97_oakleaf","rhs_weap_m70b3n_pbg40",["","","",""],["rhs_30Rnd_762x39mm_tracer","rhs_VOG25"],"rhs_weap_tt33",["","","",""],["rhs_mag_762x25_8"]],
		["I_Soldier_AR_F","rhssaf_uniform_m93_oakleaf","rhssaf_vest_md99_md2camo_fix","rhssaf_kitbag_digital","rhssaf_helmet_m59_85_nocamo","rhs_weap_m84",["","","",""],["rhs_100Rnd_762x54mmR_green"],"rhs_weap_tt33",["","","",""],["rhs_mag_762x25_8"]],
		["I_medic_F","rhssaf_uniform_m93_oakleaf","rhssaf_vest_md99_md2camo_rifleman_fix","rhs_assault_umbts","rhssaf_helmet_m97_oakleaf","rhs_weap_m70b3n",["","","",""],["rhs_30Rnd_762x39mm_tracer"],"rhs_weap_tt33",["","","",""],["rhs_mag_762x25_8"]],
		["I_G_medic_F","rhssaf_uniform_m93_oakleaf","rhssaf_vest_md99_md2camo_rifleman_fix","B_Carryall_oli","rhssaf_helmet_m97_oakleaf","rhs_weap_m70b3n",["","","",""],["rhs_30Rnd_762x39mm_tracer"],"rhs_weap_tt33",["","","",""],["rhs_mag_762x25_8"]],
		["I_officer_F","rhssaf_uniform_m10_digital","rhssaf_vest_md12_digital","","rhssaf_beret_green","rhs_weap_m21s",["","","rhs_acc_pkas",""],["rhsgref_30rnd_556x45_m21"],"",["","","",""],[]],
		["I_Soldier_SL_F","rhssaf_uniform_m93_oakleaf","rhssaf_vest_md12_digital_fix","rhs_assault_umbts","rhssaf_helmet_m97_oakleaf","rhs_weap_m70b3n_pbg40",["","","",""],["rhs_30Rnd_762x39mm_tracer","rhs_VOG25"],"rhs_weap_tt33",["","","",""],["rhs_mag_762x25_8"]]]
	],
	["Talib","Terrorist 01","Madin",[
		["I_soldier_F","LOP_U_BH_Fatigue_CHOCO","LOP_V_CarrierLite_TAN","","H_ShemagOpen_khk","rhs_weap_MP44",["","","",""],["rhsgref_30Rnd_792x33_SmE_StG"],"rhs_weap_tt33",["","","",""],["rhs_mag_762x25_8"]],
		["I_crew_F","LOP_U_BH_Fatigue_CHOCO","LOP_V_CarrierLite_TAN","","","rhs_weap_MP44",["","","",""],["rhsgref_30Rnd_792x33_SmE_StG"],"rhs_weap_tt33",["","","",""],["rhs_mag_762x25_8"]],
		["I_Soldier_TL_F","LOP_U_BH_Fatigue_GUE_TRI_TAN","LOP_V_CarrierRig_TAN","","LOP_H_Shemag_TAN","rhs_weap_MP44",["","","",""],["rhsgref_30Rnd_792x33_SmE_StG"],"rhs_weap_tt33",["","","",""],["rhs_mag_762x25_8"]],
		["I_Soldier_AR_F","LOP_U_BH_Fatigue_CHOCO_TRI","LOP_V_CarrierLite_TAN","B_AssaultPack_cbr","LOP_H_Shemag_OLV","rhs_weap_mg42",["","","",""],["rhsgref_50Rnd_792x57_SmE_drum"],"rhs_weap_tt33",["","","",""],["rhs_mag_762x25_8"]],
		["I_medic_F","LOP_U_BH_Fatigue_ACU_CHOCO","LOP_V_CarrierLite_TAN","B_AssaultPack_cbr","LOP_H_Shemag_RED2","rhs_weap_MP44",["","","",""],["rhsgref_30Rnd_792x33_SmE_StG"],"rhs_weap_tt33",["","","",""],["rhs_mag_762x25_8"]],
		["I_G_medic_F","LOP_U_BH_Fatigue_CHOCO","LOP_V_CarrierLite_TAN","B_Carryall_cbr","LOP_H_Shemag_RED1","rhs_weap_MP44",["","","",""],["rhsgref_30Rnd_792x33_SmE_StG"],"rhs_weap_tt33",["","","",""],["rhs_mag_762x25_8"]],
		["I_Soldier_SL_F","LOP_U_BH_Fatigue_CHOCO","LOP_V_CarrierRig_TAN","B_AssaultPack_cbr","H_ShemagOpen_khk","rhs_weap_MP44",["","","",""],["rhsgref_30Rnd_792x33_SmE_StG"],"rhs_weap_tt33",["","","",""],["rhs_mag_762x25_8"]],
		["I_officer_F","LOP_U_BH_Fatigue_CHOCO","LOP_V_CarrierRig_TAN","tfw_ilbe_dd_coy","H_ShemagOpen_khk","rhs_weap_m79",["","","",""],["rhs_mag_M441_HE"],"rhs_weap_savz61_folded",["","","",""],["rhsgref_20rnd_765x17_vz61"]]]
	]
];